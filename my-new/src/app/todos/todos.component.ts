import { Component, OnInit } from '@angular/core';
import { ToDoService } from '../todos/todo-list/todo.service';
import { ToDo } from '../todos/todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent {
  appTodos: ToDo[];
  constructor(private todoService: ToDoService) {
    this.appTodos = this.todoService.getTodos();
  }

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
    this.appTodos = this.todoService.getTodos();
  }
  completeTodo(item: { todo: ToDo }) {
    this.todoService.completeToDo(item.todo);
    this.appTodos = this.todoService.getTodos();
  }

  removeTodo(item: { todo: ToDo }) {
    this.todoService.removeTodo(item.todo);
    this.appTodos = this.todoService.getTodos();
  }
}
