import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todoform',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent {
  label: string;
  @Output() OnAdd = new EventEmitter();

  submit() {
    if (!this.label) {
      return;
    }
    this.OnAdd.emit({ label: this.label });
    this.label = '';
  }
}
