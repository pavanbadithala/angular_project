import { Injectable } from '@angular/core';
import { ToDo } from '../todo';

@Injectable()
export class ToDoService {
  todos: ToDo[] = [
    {
      label: 'java script',
      id: 0,
      complete: false
    },
    {
      label: 'typescript',
      id: 1,
      complete: false
    },
    {
      label: 'angular 4',
      id: 2,
      complete: false
    },
    {
      label: 'reactjs',
      id: 3,
      complete: false
    }
  ];

  getTodos() {
    return this.todos;
  }

  addTodo(item: { label: string }) {
    this.todos = [
      {
        label: item.label,
        id: this.todos.length + 1,
        complete: false
      },
      ...this.todos
    ];
  }

  completeToDo(todo: ToDo) {
    this.todos = this.todos.map(
      item => (item.id === todo.id ? { ...item, complete: true } : item)
    );
  }

  removeTodo(todo: ToDo) {
    this.todos = this.todos.filter(it => it.id !== todo.id);
  }
}
