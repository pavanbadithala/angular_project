import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todolist',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {
  @Input() todos;
  @Output() OnComplete = new EventEmitter();
  @Output() OnDelete = new EventEmitter();
}
