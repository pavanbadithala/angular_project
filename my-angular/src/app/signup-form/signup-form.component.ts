import { Component, PipeTransform } from '@angular/core';
import { User } from '../signup.interface';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent {
  user: User = {
    name: '',
    account: {
      email: '',
      confirm: '',
      mobile: '',
    }
  };
  onSubmit({ value, valid }: { value: User; valid: boolean }) {
    console.log(value, valid);
  }
}
