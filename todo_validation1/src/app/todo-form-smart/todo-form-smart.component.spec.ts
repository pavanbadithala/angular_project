import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoFormSmartComponent } from './todo-form-smart.component';

describe('TodoFormSmartComponent', () => {
  let component: TodoFormSmartComponent;
  let fixture: ComponentFixture<TodoFormSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoFormSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoFormSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
